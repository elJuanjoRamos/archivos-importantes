import os
import time

import geckodriver_autoinstaller
from selenium import webdriver
from selenium.webdriver.support.select import Select
import datetime
import platform


path = os.path.dirname(os.path.abspath(__file__))


def _getElement(name, driver):
    try:
        element = driver.find_element_by_id(name)
        return element
    except Exception as e:
        raise e


def test1(drive):
    time.sleep(4)
    print("Inicia Cambio NOmbre...")

    try:

        element = _getElement('name', drive)
        element.clear()
        element.send_keys('prueba selenium')
        time.sleep(1)
        button = _getElement('button', drive)
        button.click()
        time.sleep(1)
        print("Finaliza Prueba")

    except Exception as e:
        raise e

def test2(drive):
    time.sleep(4)
    print("Inicia Cambio Carnet...")

    try:
        element = _getElement('carnet', drive)
        element.clear()
        element.send_keys('0000000')
        time.sleep(1)
        button = _getElement('button', drive)
        button.click()
        time.sleep(1)
        print("Finaliza Prueba")

    except Exception as e:
        raise e


def _getDriver(url):
    geckodriver_autoinstaller.install()
    option = webdriver.FirefoxOptions()
    option.add_argument("--window-size={w},{h}".format(w=str(900), h=str(700)))
    #option.add_argument("--headless")
    driver = webdriver.Firefox(options=option)
    driver.get(url)
    return driver


if __name__ == '__main__':
    print('... running the script')
    print('currently time : ', datetime.datetime.now())
    print('currently Platform: ', platform.platform())

    driver = _getDriver('http://localhost:4200')
    test1(driver)
    test2(driver)
    driver.close()
    print('Finalizar  puebas Funcionales: Ok')
