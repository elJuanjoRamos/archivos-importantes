# Practica AYD



## Antes de empezar

Se debe contar con una cuenta activa en google cloud

Se debe contar con una cuenta en cualquier otro proveedor de servicios en la nube, como AWS o Digital Ocean



## Preparar el entorno

1. Dirigirse a su cuenta de google y cree un cluste de Kubernetes en el apartado "Kubernetes Engine"

2. Cree una instancia de maquina virtual en su proveedor de servicios.

3. Conectese a su maquina virtual mediante SSH

4. Instalar Gitlab Runner para tener conexion con el repositorio. Cuando instalamos gitlab runner en una maquina también se crea el usuario "gitlab-runner" que es el usuario que ejecutará todo el pipeline en la máquina. 

```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

sudo apt-get install gitlab-runner

```

5. Instalar Docker, para poder descargar las imagenes correspondientes que seran administradas por el cluster de Kubernetes

* Remover versiones anteriores de Docker

```
sudo apt-get remove docker docker-engine docker.io containerd runc
```
* Actualice el índice de paquetes de apt e instale paquetes para permitir que apt use un repositorio a través de HTTPS

```
sudo apt-get update

sudo apt-get install ca-certificates curl gnupg lsb-release
```
* Agrege la llave oficial de GPG de Docker
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```
* Utilice el siguiente comando para configurar el repositorio estable. Para agregar el repositorio nightly o test.

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
* Instalar Docker Engine
```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
* Verifique que Docker Engine esté instalado correctamente ejecutando la imagen hello-world.

```
sudo docker run hello-world
```

6. Quitarle la necesidad de contraseña al usuario gitlab-runner

Esto es únicamente para que podamos hacer pruebas nosotros como el usuario "gitlab-runner", puesto que debemos cambiar del usuario "ubuntu".

* Escribir el siguiente comando
```
sudo nano /etc/pam.d/su
```
* Esto abrira un archivo con configuraciones del sistema, dentro de ese archivo, justo debajo de la linea que dice "auth sufficient pam_rootok.so", pegue las siguientes lineas

```
auth       [success=ignore default=1] pam_succeed_if.so user = gitlab-runner
auth       sufficient   pam_succeed_if.so use_uid user ingroup gitlab-runner
```
Presiones las teclas Ctr + X para salir, el sistema solicitara guardar el archivo, acepte presionando Enter y luego Enter de nuevo para guardar con el mismo nombre.

* Agregar el usuario gitlab-runner al grupo de usuarios Root

```
sudo usermod -aG gitlab-runner ubuntu
```

* Darle permisos de super usuario al usuario gitlab-runner

```
sudo usermod -aG gitlab-runner $USER
newgrp gitlab-runner

```

7. Permitimos al usuario gitlab-runner ejecutar sudo sin password

Necesitamos que el usuario gitlab runner pueda ejecutar sudo y no deba ingresar la contraseña cada vez que lo hace.

Accedemos al archivo de configuraciones y le damos permisos de sudo y que pueda ejecutar sudo sin password

* Entramos al archivo de configuraciones

```
sudo visudo

```

* Localice la linea que dice "root ALL=(ALL:ALL) ALL" y justo debajo pegue la siguiente linea
```
gitlab-runner ALL=(ALL:ALL) ALL
```

* Localice la linea que dice "#includedir /etc/sudoers.d" y justo debajo pegue la siguiente linea
```
gitlab-runner ALL=(ALL:ALL) NOPASSWD:ALL
```

* El archivo de configuracion debe verse como el siguiente, observe las lineas corespondientes donde se encunentra el usuario gitlab-runner
```
#
# This file MUST be edited with the 'visudo' command as root.
#
# Please consider adding local content in /etc/sudoers.d/ instead of
# directly modifying this file.
#
# See the man page for details on how to write a sudoers file.
#
Defaults        env_reset
Defaults        mail_badpass
Defaults        secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL
gitlab-runner ALL=(ALL:ALL) ALL
# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d
gitlab-runner ALL=(ALL:ALL) NOPASSWD:ALL

```
* Guarde las configuraciones del archivo

* Cambien al usuario gitlab-runner con el siguiente comando

```
su - gitlab-runner

```
Si todo resulto exitoso, deberia de ver en la consola que ahora el usuario seleccionado es gitlab runner

```
gitlab-runner@ip-172-31-36-44:~$ 

```

**Es importante que el usuario gitlab-runner pueda acceder sin password, dado que el resto de pasos depende de ello**


## Instalar GCloud

**Recomendación: Realizar las instalaciones con el usuario gitlab-runner**

1. Agrega el URI de distribución del SDK de Cloud como una fuente de paquete:

```
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
```
2. Asegúrate de que tengas instalado apt-transport-https

```
sudo apt-get install apt-transport-https ca-certificates gnupg
```
3. Importa la clave pública de Google Cloud

```
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
```

4. Instala y actualiza el SDK de Cloud:

```
sudo apt-get update && sudo apt-get install google-cloud-sdk
```

5. Ejecuta gcloud init para comenzar y conectese a su cuenta de google donde tenga configurado el cluster de kubernetes

```
gcloud init
```

**Recomendación: Cuando seleccione el proyecto al cual conectarse y aparezca la siguiente pregunta "Do you want to configure a default Compute Region and Zone", presione la tecla n para tener la configuracion por default**


## Instalar Kubectl

**Recomendación: Realizar las instalaciones con el usuario gitlab-runner**

1. Descargar la version latest con el siguiente comando

```
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
```
2. Validar el binario (opcional). Descargue el archivo de suma de comprobación de kubectl

```
curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
```
* Validar el binario kubectl con el archivo de suma de comprobación
```
echo "$(<kubectl.sha256) kubectl" | sha256sum --check
```
* Si es valido, la salida deberia de decir 

```
kubectl: OK
```

3. Instalar Kubectl

```
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
```

4. Pruebe para asegurarse de que la versión que instaló esté actualizada

```
kubectl version --client
```
Deberia de mostrar una salida simila a esta
```
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.3", GitCommit:"c92036820499fedefec0f847e2054d824aea6cd1", GitTreeState:"clean", BuildDate:"2021-10-27T18:41:28Z", GoVersion:"go1.16.9", Compiler:"gc", Platform:"linux/amd64"}
```

5. Conectamos el usuario gitlab-runner al cluster, este comando lo obtienes en la opción "Conectar" del cluster.

* Dirijase a su cuenta de google y dirijase a su cluster, dentro del cluster dirijase a "conectar", esto le proporcionara una cadena de conexion similar a la siguiente

```
gcloud container clusters get-credentials practica --zone us-central1-c --project sopes-328302
```

* Pegar la cadena de conexion en la maquina virtual, esto deberia de crear una conexion entrela maquina virtual y el cluster, si todo resulto exitoso, deberia de aparecer un mensaje como el siguiente.


```
Fetching cluster endpoint and auth data.
kubeconfig entry generated for "nombre_de_cluster".

```

## Creando un Runner de gitlab para relizar pipelines

1. Dentro de su repositorio, dirijase a "Settings" y luego a "CI/CD".

2. Dentro de "CI/CD" dirijase al apartado "Runners", haga click en el boton "Expand"
    * Dentro podra observar unas instrucciones de registro, pero lo mas importante son dos cosas, la url y el token de registro que es propio del repositorio

3. Dirijase a su maquina virtual y sobre el usuario gitlab-runner ejecutar el comando

```
sudo gitlab-runner register
```
Al hacerlo le solicitara la url del proyecto, copiela y peguela desde las instrucciones comentadas con anterioridad, dentro de la maquina virtual deberia verse similar a esto

```
gitlab-runner@ip-172-31-36-44:~$ sudo gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=53522 revision=4b9e985a version=14.4.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
"TOCKEN DE GITLAB"
```


4. Agregar una descripcion para el runner, esto es opcional, si no desea agergar descripcion, haga click en Enter

5. Agregar una etiqueta para el runner, esto es opcional, si no desea agergar etiqueta, haga click en Enter
    * **Recomendación: Para evitar pasos extra, agrege una etiqueta sencilla para hacer referencia al runner en pasos posteriores**

6. **Importante** Seleccionar un ejecutor para el runner, el ejecutor es la forma en como el runner realizara el pipeline
    * El ejecutor debe ser tipo **shell**

7. Si todo resulto exitoso, deberia de mostrar un mensaje como este

```
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
gitlab-runner@ip-172-31-36-44:~$

```
8. Dirijase nuevamente al repositorio y en la parte de Runners deberia de poder visualizar el runner que acaba de crear

# Configurar una IP estática en Google Cloud

Confiturar una ip estatica serivira para crear un balanceador de carga que levante sobre esa ip y evitar que el balanceador genere una ip dinamicamente, esto ayuda a que sabiendo la ip del balanceador, es posible realizar pruebas o conectarse directamente al mismo sin tener que esperar a que este genere una propia.

1. Dirijase al apartado "Red de VPC" de proyecto

2. Una vez dentro, dirijase al apartado "Direcciones IP externas"

3. Haga click en el boton "Reservar una dirección estática"
    * Dentro de la configuracion se le solicitara un nombre para la ip
    * Nivel de servicio de red seleccione **Premium**
    * La version de IP seleccione **IPv4**
    * El tipo seleccione **Regional**



# Archivos de Kubernetes 

Es necesario crear la estructura de los archivos que se ejecutaran como parte del despliegue en kubernetes, dentro del proyecto cree una carpeta llamada kubernetes y dentro, cree los siguietes archivos.

* Un archivo yaml llamado deploy-front.yaml, dicho archivo debe tener la siguiente estructura
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deploy-backend
spec:
  replicas: 2
  selector:
    matchLabels:
      tag: label-backend
  template:
    metadata:
      labels:
          tag: label-backend
    spec:
      containers:
      - image: eljuanjoramos/pagina-web
        name: pagina-web
```
* Un archivo yaml llamado load-balancer.yaml, dicho archivo debe tener la siguiente estructura

```
apiVersion: v1
kind: Service
metadata:
    name: svc-201801262
spec:
    loadBalancerIP: 130.211.211.66
    type: LoadBalancer
    selector:
        tag: label-backend
    ports:
    - name: port-front
      protocol: TCP
      port: 80 # this is the service port
      targetPort: 80  # this is the container port
```

Como se puede observar, dentro de este archivo, se crea un balanceador de carga que levantara el la pagina web, a dicho balanceador se le configuro la ip estatica reservada con anterioridad para acceder directamente a dicho balanceador y a los pods de la pagina web


# Configuracion de pruebas funcionales

Dentro de la carpeta **selenium_grid** se encuentra todo lo necesario para la realizacion de pruebas funcionales sobre la pagina web, dentro de la carpeta **test** se encuentra un archivo llamado **google_test.py** dicho archivo es el que se corre para la realizacion de pruebas.


**Importante: Es necesario que toda prueba funcional realizada tenga un nombre de funcion con la siguiente estructura test_funtionName** 

Esto es debido a que sin la palabra test, selenium grid no detectara la funcion como una prueba y no la ejecutara.

**Importante: Es necesario que toda prueba funcional se haga referencia a que se ejecutara sobre el balanceador de carga actual, porl lo tanto hay que indicar que se ira a ejecutar sobre la ip del balanceador** 

Por ejemplo:

```
def test_changeName(remote_browser):
    remote_browser.get('http://130.211.211.66')
    time.sleep(4)
    print("Inicia Cambio nombre...")

    try:

        element = remote_browser.find_element_by_id('name')
        element.clear()
        element.send_keys('prueba selenium')
        time.sleep(1)
        button = remote_browser.find_element_by_id('button')
        button.click()
        time.sleep(1)
        print("Finaliza Prueba")

    except Exception as e:
        raise e
```

Como se puede observar, la funcion incluye la palabra test sobre su nombre asi como tambien, le indica que ira a ejecutar dicha prueba sobre la pagina montada en la ip **130.211.211.66** que es la ip del balanceador.



# Configuracion de .gitlab-ci.yml 

El archivo gitlab-ci.yml es el ecargado de realizar el pipeline, dicho archivo debe estar configurado de la siguiente manera si es la primera vez que se corre

```
stages:          # List of stages for jobs, and their order of execution
  - kubernetes_deploy

#---------------DEPLOY A KUBERNETES-------------------

kubernetes-test:
  stage: kubernetes_deploy
  script:
    #Eliminar imagenes previas y obtener actuales
    - sudo docker rmi eljuanjoramos/pagina-web -f
    
    #Descargar images  actuales
    - sudo docker pull eljuanjoramos/pagina-web
    # Borrar repositorio
    - sudo rm -rf repo

    # clonar repositorio
    - sudo mkdir -p repo
    - cd repo
    - sudo git clone https://gitlab.com/elJuanjoRamos/archivos-importantes.git
    - cd archivos-importantes/kubernetes
    # conexion al cluster
    - gcloud container clusters get-credentials practica --zone us-central1-c --project sopes-328302
    - kubectl get svc
    - kubectl get pods
    - kubectl create -f deploy-front.yaml
    - kubectl apply -f load-balancer.yaml
    - kubectl get svc
  tags:
    - "practica"

```

Sin embargo, una vez creado el servicio del load balancer por primera vez, ya se podra configurar para ejecutar las pruebas funcionales por lo tanto, es necesario cambiar la estructura del archivo a la siguiente

```
stages:          # List of stages for jobs, and their order of execution
  - chrome_test
  - firefox_test
  - kubernetes_deploy



#---------------PRUEBAS FUNCIONALES-------------------
test-selenium-firefox:
  stage: firefox_test
  image: python:3.7.6-alpine
  services:
    - selenium/standalone-firefox
  before_script:
    - apk add --no-cache python3 py3-pip
  script:
    - cd selenium_grid
    - python3 -m venv env
    - source env/bin/activate
    - pip3 install -r requirements.txt
    - pytest --browser=firefox --local='false'


test-selenium-chrome:
  stage: chrome_test
  image: python:3.7.6-alpine
  services:
    - selenium/standalone-chrome
  before_script:
    - apk add --no-cache python3 py3-pip
  script:
    - cd selenium_grid
    - python3 -m venv env
    - source env/bin/activate
    - pip3 install -r requirements.txt
    - pytest --browser=chrome --local='false'



#---------------DEPLOY A KUBERNETES-------------------

kubernetes-test:
  stage: kubernetes_deploy
  script:
    #Eliminar imagenes previas y obtener actuales
    - sudo docker rmi eljuanjoramos/pagina-web -f
    
    #Descargar images  actuales
    - sudo docker pull eljuanjoramos/pagina-web
    # Borrar repositorio
    - sudo rm -rf repo

    # clonar repositorio
    - sudo mkdir -p repo
    - cd repo
    - sudo git clone https://gitlab.com/elJuanjoRamos/archivos-importantes.git
    - cd archivos-importantes/kubernetes
    # conexion al cluster
    - gcloud container clusters get-credentials practica --zone us-central1-c --project sopes-328302
    # eliminar servicios load balancer
    - kubectl delete svc svc-201801262
    - kubectl delete deployments deploy-backend
    - kubectl get svc
    - kubectl get pods
    - kubectl create -f deploy-front.yaml
    - kubectl apply -f load-balancer.yaml
    - kubectl get svc

  tags:
    - "practica"

```
Como se puede observar, dentro del estado de kubernetes, al final de la instruccion, hay una linea que dice **tag** esto es importante porque ahi es donde se hace referencia al runner en la nube, la tag debe ser la misma que tiene asociado el runner para indicar que sobre esa maquina se va a ejecutar el pipeline, por ello la importancia de que el runner tuviera una tag sencilla de recordar


**Importante: A veces, el pipeline ejecutado en la maquina deja una especie de cache que impide ejecutar otro pipeline, para corergir dicho error, dirijase a su maquina virtual, entre como usuario gitlab-runner y ejecute un comando ls, observara que existe una carpeta llamada builds, dentro de ella, hay otra carpeta con nombre raro que representa la cache, elimine dicha carpeta con el comado sudo rm -rf "nombre_del_directorio"**




