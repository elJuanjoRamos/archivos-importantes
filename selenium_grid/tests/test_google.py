from pytest import raises
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
import time
import os


def test_changeName(remote_browser):
    remote_browser.get('http://130.211.211.66')
    time.sleep(4)
    print("Inicia Cambio nombre...")

    try:

        element = remote_browser.find_element_by_id('name')
        element.clear()
        element.send_keys('prueba selenium')
        time.sleep(1)
        button = remote_browser.find_element_by_id('button')
        button.click()
        time.sleep(1)
        print("Finaliza Prueba")

    except Exception as e:
        raise e


def test_changeCarnet(remote_browser):
    remote_browser.get('http://130.211.211.66')
    time.sleep(4)
    print("Inicia Cambio carnet...")

    try:

        element = remote_browser.find_element_by_id('carnet')
        element.clear()
        element.send_keys('201801262')
        time.sleep(1)
        button = remote_browser.find_element_by_id('button')
        button.click()
        time.sleep(1)
        print("Finaliza Prueba")

    except Exception as e:
        raise e

