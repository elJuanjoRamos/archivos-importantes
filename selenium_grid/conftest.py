
import pytest
from selenium import webdriver
from selenium.webdriver import Remote


def pytest_addoption(parser):
    parser.addoption('--browser', help='Which test browser?',
                     default='chrome')
    parser.addoption('--local', help='local or CI?',
                     choices=['true', 'false'],
                     default='true')


@pytest.fixture(scope='session')
def test_browser(request):
    return request.config.getoption('--browser')


@pytest.fixture(scope='session')
def local(request):
    return request.config.getoption('--local')


@pytest.fixture(scope='function')
def remote_browser(test_browser, local) -> Remote:
    if local != 'true' and local != 'false':
        raise ValueError(f'--local={local}". Driver could not be setup.\n'
                         'pass "true" if local execute\n'
                         'pass "false" if use CI service')
    cmd_executor = {
        'true': 'http://localhost:4444/wd/hub',
        'false': f'http://selenium__standalone-{test_browser}:4444/wd/hub'
    }
    if test_browser == 'firefox':
        driver = webdriver.Remote(
            options=webdriver.FirefoxOptions(),
            command_executor=cmd_executor[local])
    elif test_browser == 'chrome':
        driver = webdriver.Remote(
            options=webdriver.ChromeOptions(),
            command_executor=cmd_executor[local])
    else:
        raise ValueError(
            f'--browser="{test_browser}" is not chrome or firefox')
    yield driver
    driver.quit()
